<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
  <id>org.gnome.font-viewer.desktop</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0+</project_license>
  <name>Fonts</name>
  <summary>View fonts on your system</summary>
  <description>
    <p>
      Fonts shows you the fonts installed on your computer for your use as
      thumbnails.
      Selecting any thumbnails shows the full view of how the font would look under
      various sizes.
    </p>
    <p>
      Fonts also supports installing new font files downloaded in the .ttf
      and other formats.
      Fonts may be installed only for your use or made available to all users on the computer.
    </p>
  </description>
  <developer_name>The GNOME Project</developer_name>
  <url type="homepage">https://www.gnome.org/</url>
  <url type="bugtracker">https://gitlab.gnome.org/GNOME/gnome-font-viewer/issues</url>
  <url type="donation">https://www.gnome.org/donate/</url>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/GNOME/gnome-font-viewer/raw/HEAD/data/screenshots/fonts-1.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/GNOME/gnome-font-viewer/raw/HEAD/data/screenshots/fonts-2.png</image>
    </screenshot>
  </screenshots>
  <project_group>GNOME</project_group>
  <kudos>
    <kudo>AppMenu</kudo>
    <kudo>ModernToolkit</kudo>
  </kudos>
  <content_rating type="oars-1.1" />
  <releases>
    <release version="42.0" date="2022-03-20">
      <description>
        <p>Fonts 42.0 comes with a port to GTK4 and the following translation updates:</p>
        <ul>
          <li>Basque</li>
          <li>Bulgarian</li>
          <li>Catalan</li>
          <li>Chinese (China)</li>
          <li>Croatian</li>
          <li>Czech</li>
          <li>Finnish</li>
          <li>French</li>
          <li>Friulian</li>
          <li>Galician</li>
          <li>Icelandic</li>
          <li>Indonesian</li>
          <li>Korean</li>
          <li>Lithuanian</li>
          <li>Occitan (post 1500)</li>
          <li>Persian</li>
          <li>Polish</li>
          <li>Portuguese</li>
          <li>Portuguese (Brazil)</li>
          <li>Russian</li>
          <li>Slovak</li>
          <li>Slovenian</li>
          <li>Spanish</li>
          <li>Swedish</li>
          <li>Turkish</li>
          <li>Ukrainian</li>
        </ul>
      </description>
    </release>
    <release version="41.0" date="2021-09-21">
      <description>
        <p>Fonts 41 comes with the following translation updates:</p>
        <ul>
          <li>Catalan</li>
          <li>Chinese (China)</li>
          <li>Friulian</li>
          <li>Hebrew</li>
          <li>Occitan</li>
          <li>Russian</li>
        </ul>
      </description>
    </release>
    <release version="40.0" date="2021-03-20">
      <description>
        <p>Fonts 40 comes with a slight visual refresh, sporting rounded bottom window corners. In addition there have been general bug fixes and improvements.</p>
        <ul>
          <li>The About dialog now provides a link to the repository (jridehalgh)</li>
          <li>Startup should be faster now that we use font-config's APIs directly (Evan Welsh)</li>
        </ul>
        <p>Fonts 40 also comes with the following translation updates:</p>
        <ul>
          <li>Basque</li>
          <li>Catalan</li>
          <li>Chinese (China)</li>
          <li>Chinese (Taiwan)</li>
          <li>Danish</li>
          <li>Dutch</li>
          <li>Friulian</li>
          <li>Galician</li>
          <li>Greek, Modern (1453-)</li>
          <li>Japanese</li>
          <li>Kazakh</li>
          <li>Kurdish, Central</li>
          <li>Malay</li>
          <li>Panjabi</li>
          <li>Persian</li>
          <li>Portuguese</li>
          <li>Portuguese (Brazil)</li>
          <li>Slovak</li>
          <li>Ukrainian</li>
        </ul>
      </description>
    </release>
    <release version="3.34.0" date="2019-09-14">
      <description>
        <ul>
          <li>Improve compatibility for fonts with non-unicode charmaps</li>
          <li>Display an error dialog when installation fails</li>
          <li>Fix font installation when filenames would collide</li>
          <li>Fix thumbnailing for font collection files</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="3.32.0" date="2019-03-15">
      <description>
        <ul>
          <li>Update the application icon</li>
          <li>Improve usability of the Install button</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="3.30.0" date="2018-08-30">
      <description>
        <ul>
          <li>Show more OpenType features</li>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
    <release version="3.28.0" date="2018-03-13">
      <description>
        <ul>
          <li>Translation updates</li>
        </ul>
      </description>
    </release>
  </releases>
  <translation type="gettext">gnome-font-viewer</translation>
</component>
